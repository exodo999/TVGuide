﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Logging;
using TVGuide.Models;
// For more information on enabling MVC for empty projects, visit https://go.microsoft.com/fwlink/?LinkID=397860

namespace TVGuide
{
    [ApiController]
    [Route("[controller]")]
    public class TVChannelController : ControllerBase
    {

        // GET: /<controller>/
        [HttpGet]
        public TVChannel Get()
        {
            TVChannel channel = new TVChannel()
            {
                ChannelId = 1,
                ChannelName = "TV Channel 1"
            };

            return channel;
        }
    }
}
