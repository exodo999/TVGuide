﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVGuide.Models
{
    public class TVChannel
    {
        public int ChannelId { get; set; }
        public  string ChannelName { get; set; }
        public  ICollection<TVProgram> TVPrograms { get; set; }
    }
}
