﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace TVGuide.Models
{
    public class TVProgram
    {
        TVChannel Channel { get; set; }
        DateTime ProgramStartDateTime { get; set; }
        DateTime ProgramEndDateTime { get; set; }
        string ProgramName { get; set; }
    }
}
